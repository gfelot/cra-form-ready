import React from 'react';

const ButtonsGroup = ({ buttonOne, buttonTwo, twoButton }) => (
  <div className="columns">
    <div className="column">
      <div className="field is-grouped is-grouped-right">
        {twoButton && (
          <p className="control">
            <button
              className={buttonOne.className}
              type={buttonOne.type}
              disabled={buttonOne.disabled}
              onClick={buttonOne.onClick}
            >
              {buttonOne.text}
            </button>
          </p>
        )}
        <p className="control">
          <button
            className={buttonTwo.className}
            type={buttonTwo.type}
            disabled={buttonTwo.disabled}
            onClick={buttonTwo.onClick}
          >
            {buttonTwo.text}
          </button>
        </p>
      </div>
    </div>
  </div>
);

export default ButtonsGroup;
