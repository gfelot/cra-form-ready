import React from 'react';
import { translate } from 'react-i18next';

let Modal = ({ onClick, t }) => (
  <div className="modal is-active" >
    <div className="modal-background"></div>
    <div className="modal-card">
      <header className="modal-card-head">
        <h1 className="modal-card-title title has-text-grey-dark">{t('cgu:mention')}</h1>
        {/* <button className="delete" aria-label="close"></button> */}
      </header>
      <section className="modal-card-body">
        <div className="content">
          <h2 class="title is-5 has-text-grey-dark">
            <u>
              {t('cgu:editor.title')}
            </u>
          </h2>
          <h3 className="title is-7 has-text-justified has-text-grey-dark">
            {t('cgu:editor.text')}
          </h3>

          <h2 class="title is-5 has-text-grey-dark">
            <u>
              {t('cgu:director.title')}
            </u>
          </h2>
          <h3 className="title is-7 has-text-justified has-text-grey-dark">
            {t('cgu:director.text')}
          </h3>

          <h2 class="title is-5 has-text-grey-dark">
            <u>
              {t('cgu:webmaster.title')}

            </u>
          </h2>
          <h3 className="title is-7 has-text-justified has-text-grey-dark">
            <u>
              {t('cgu:webmaster.text')}
            </u>
          </h3>

          <h2 class="title is-5 has-text-grey-dark">
            <u>
              {t('cgu:host.title')}

            </u>
          </h2>
          <h3 className="title is-7 has-text-justified has-text-grey-dark">
            {t('cgu:host.text')}
          </h3>

          <h2 class="title is-5 has-text-grey-dark">
            <u>
              {t('cgu:copyright.title')}
            </u>
          </h2>
          <h3 className="title is-7 has-text-justified has-text-grey-dark">
            {t('cgu:copyright.text')}
          </h3>

          <h2 class="title is-5 has-text-grey-dark">
            <u>
              {t('cgu:credits.title')}
            </u>
          </h2>
          <h3 className="title is-7 has-text-justified has-text-grey-dark">
            {t('cgu:credits.text')}
          </h3>

          <h2 class="title is-5 has-text-grey-dark">
            <u>
              {t('cgu:law.title')}
            </u>
          </h2>

          <h3 className="title is-7 has-text-justified has-text-grey-dark">

            {t('cgu:law.text1')}
            <br /><br />
            {t('cgu:law.text2')}
            <br /><br />
            {t('cgu:law.address')}
            <br />
            {t('cgu:law.street')}
            <br />
            {t('cgu:law.other')}
            <br />
            {t('cgu:law.country')}
            <br /><br />
            {t('cgu:law.text3')}
            <br /><br />
            {t('cgu:law.text4')}

          </h3>

          <h2 class="title is-5 has-text-grey-dark">
            <u>
              {t('cgu:resp.title')}
            </u>
          </h2>
          <h3 className="title is-7 has-text-justified has-text-grey-dark">
            {t('cgu:resp.text1')}
          </h3>

          <ul className="title is-7 has-text-justified has-text-grey-dark">
            <li>{t('cgu:resp.list1')}</li>
            <li>{t('cgu:resp.list2')}</li>
            <li>{t('cgu:resp.list3')}</li>
            <li>{t('cgu:resp.list4')}</li>
          </ul>

          <h3 className="title is-7 has-text-justified has-text-grey-dark">
            {t('cgu:resp.text2')}
          </h3>

          <h2 class="title is-5 has-text-grey-dark">
            <u>
              {t('cgu:cgu')}
            </u>
          </h2>

          <h2 class="title is-5 has-text-grey-dark">
            <u>
              {t('cgu:object.title')}
            </u>
          </h2>
          <h3 className="title is-7 has-text-justified has-text-grey-dark">
            {t('cgu:object.text')}
          </h3>

          <h2 class="title is-5 has-text-grey-dark">
            <u>
              {t('cgu:geoloc.title')}
            </u>
          </h2>
          <h3 className="title is-7 has-text-justified has-text-grey-dark">
            {t('cgu:geoloc.text1')}
            <br />
            {t('cgu:geoloc.text2')}
          </h3>
          <br />
        </div>
      </section>
      <footer className="modal-card-foot">
        <button className="button is-success" onClick={onClick}>J'ai lu</button>
      </footer>
    </div>
  </div>
);

Modal = translate()(Modal)

export default Modal;