import React from 'react';

const TextArea = ({ placeholder, rows, label }) => (
  <div className="field">
    <label className="label">{label}</label>
    <textarea className="textarea" placeholder={placeholder} rows={rows} />
  </div>
);

export default TextArea;
