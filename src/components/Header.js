import React from 'react';
import { translate } from 'react-i18next';

let Header = ({ t }) => {
  const style = {
    maxHeight: '100%'
  };
  return (
    <div className="hero-head">
      <div className="container">
        <nav className="navbar level">

          <div className="level-left">
          <p class="level-item">
            <a class="link is-info">Home</a>
          </p>
          </div>
          <p class="level-item">
            <h1 class="title">{t('title')}</h1>
          </p>
          <div className="level-right">
            <p class="level-item">
              <a class="link is-info">Home</a>
            </p>
          </div>

        </nav>
      </div>
    </div>
  );
};

Header = translate()(Header);

export default Header;
