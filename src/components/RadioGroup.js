import React from 'react';
import { Field } from 'redux-form';
import uuid from 'uuid/v4';


const RadioGroup = ({ label, name, component, type, items }) => {

  return (
    <div className="field">
      <label className="label">{label}</label>
      <div className="control has-text-black">
        {items && items.map(item => (
          <label className={type} key={uuid()}>
            <Field
              name={name}
              component={component}
              type={type}
              value={item.value}
            />
            {item.text}
          </label>
        ))}
      </div>
    </div>
  );
}

export default RadioGroup;
