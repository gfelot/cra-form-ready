import React from 'react';

const Input = ({
  input,
  label,
  placeholder,
  value,
  type,
  disabled,
  meta: { touched, error }
}) => (
  <div className="field">
    <label className="label">{label} {touched &&
      error && <span className="subtitle has-text-danger">{error}</span>}</label>
    <div className="control">
      <input
        {...input}
        className="input"
        type={type}
        disabled={disabled}
        placeholder={placeholder}
        value={value}
      />
    </div>
    
  </div>
);

export default Input;
