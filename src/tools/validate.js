export const required = value => (value ? undefined : 'Requis');

export const isEmail = value =>
  (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined);

const maxLength = max => value =>
  (value && value.length > max
    ? `Il ne peut y avoir que ${max} charactères maximum`
    : undefined);

export const isTelephoneNumber = value =>
(value && !/^(0|[0-7][0-9]{9})$/i.test(value)
    ? 'Numéro de téléphone invalide'
    : undefined);