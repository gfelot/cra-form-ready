import React, { Component, Fragment } from 'react';
import PageInfo from '../pages/PageInfo';
import PageMotif from '../pages/PageMotif';
import PageDescription from '../pages/PageDescription';
import PageCGU from '../pages/PageCGU';
import PageRecap from '../pages/PageRecap';
import PageConfirmation from '../pages/PageConfirmation';
import FormLayout from '../layouts/FormLayout';

class FormContainer extends Component {
  state = {
    page: 1
  };

  nextPage = () => {
    this.setState({ page: this.state.page + 1 });
  };

  previousPage = () => {
    this.setState({ page: this.state.page - 1 });
  };

  render() {
    const { onSubmit } = this.props;
    const { page } = this.state;

    return (
      <Fragment>
        {page === 1 && <PageInfo onSubmit={this.nextPage} />}
        {page === 2 && (
          <PageMotif
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
          />
        )}
        {page === 3 && (
          <PageDescription
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
          />
        )}
        {page === 4 && (
          <PageRecap
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
          />
        )}
        {page === 5 && (
          <PageCGU previousPage={this.previousPage} onSubmit={this.nextPage} />
        )}
        {page === 6 && <PageConfirmation onSubmit={onSubmit} />}
      </Fragment>
    );
  }
}

export default FormLayout(FormContainer);
