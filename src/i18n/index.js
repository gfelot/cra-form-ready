import i18next from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

import de from '../i18n/de.json';
import deCGU from '../i18n/deCGU.json';
import en from '../i18n/en.json';
import enCGU from '../i18n/enCGU.json';
import es from '../i18n/es.json';
import esCGU from '../i18n/esCGU.json';
import fr from '../i18n/fr.json';
import frCGU from '../i18n/frCGU.json';
import it from '../i18n/it.json';
import itCGU from '../i18n/itCGU.json';
import nl from '../i18n/nl.json';
import nlCGU from '../i18n/nlCGU.json';
import pt from '../i18n/pt.json';
import ptCGU from '../i18n/ptCGU.json';


i18next.use(LanguageDetector).init({
  resources: {
    de: {
      translations: de,
      cgu: deCGU
    },
    en: {
      translations: en,
      cgu: enCGU
    },
    es: {
      translations: es,
      cgu: esCGU
    },
    fr:{
      translations: fr,
      cgu: frCGU
    },
    it: {
      translations: it,
      cgu: itCGU
    },
    nl: {
      translations: nl,
      cgu: nlCGU
    },
    pt: {
      translations: pt,
      cgu: ptCGU
    }
  },
  ns: ['translations', 'cgu'],
  defaultNS: 'translations',
  detection: { order: ['navigator'] },
  fallbackLng: 'fr',
  // have a common namespace used around the full app
  interpolation: {
    escapeValue: false, // not needed for react!!
    formatSeparator: '.'
  },
  react: { wait: true }
});

export default i18next;
