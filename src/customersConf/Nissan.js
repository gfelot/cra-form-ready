import React, { Fragment } from 'react';
import { Field } from 'redux-form';
import Input from '../components/Input';
import RadioGroup from '../components/RadioGroup';
import TextArea from '../components/TextArea';
import { required, isTelephoneNumber } from "../tools/validate";
import { translate } from 'react-i18next';

let InfoConf = ({t}) => (
  <Fragment>
    <div className="columns" >
      <div className="column">
        <Field
          name='prenom'
          label={t('firstname')}
          placeholder={t('firstname')}
          type='text'
          component={Input}
          validate={[required]}
        />
      </div>
    </div>
    <div className="columns" >
      <div className="column">
        <Field
          name='nom'
          label={t('name')}
          placeholder={t('name')}
          type='text'
          component={Input}
          validate={[required]}
        />
      </div>
    </div>
    <div className="columns" >
      <div className="column">
        <Field
          name='tel'
          label={t('phone')}
          placeholder={t('phone')}
          type='tel'
          component={Input}
          validate={[required, isTelephoneNumber]}
        />
      </div>
    </div>
    <div className="columns" >
      <div className="column">
        <Field
          name='assistanceType'
          label={t('gc')}
          placeholder={t('gc')}
          type='text'
          component={Input}
          validate={[]}
          disabled={true}
          props={{ value: 'Nissan' }}
        />
      </div>
    </div>
  </Fragment>
)

let MotifConf = ({ t }) => (
  <Fragment>
    <div className="columns" >
      <div className="column">
        <Field
          name='motif'
          label={t('context')}
          type='text'
          component={Input}
          validate={[required]}
        />
      </div>
    </div>
    <div className="columns" >
      <div className="column">
        <Field
          name='immatriculation'
          label={t('immat')}
          type='text'
          component={Input}
          validate={[required]}
        />
      </div>
    </div>
    <div className="columns" >
      <div className="column">
        <Field
          name='localisation'
          label={t('loc')}
          type='text'
          component={Input}
          validate={[required]}
        />
      </div>
    </div>
    <div className="columns" >
      <div className="column">
        <RadioGroup
          label={t('undergroundCarPark')}
          type='radio'
          name='vehiculeSousSol'
          component='input'
          items={[
            {
              checked: true,
              text: `\xa0${t('yes')}`,
              value: true
            },
            {
              checked: false,
              text: `\xa0${t('no')}`,
              value: false
            }
          ]}
        />
      </div>
    </div>
  </Fragment>
)

let DescriptionConf = ({ t }) => (
  <Fragment>
    <div className="columns" >
      <div className="column">
        <Field
          name='kilometrage'
          label={t('kilometrage')}
          placeholder={t('kilometrage')}
          type='number'
          component={Input}
          validate={[]}
        />
      </div>
    </div>
    <div className="columns" >
      <div className="column">
        <Field
          name='description'
          label={t('symptome')}
          placeholder={t('symptome')}
          component={TextArea}
          props={{
            rows: 10
          }}
          validate={[]}
        />
      </div>
    </div>
  </Fragment>
)

InfoConf = translate()(InfoConf)
MotifConf = translate()(MotifConf)
DescriptionConf = translate()(DescriptionConf)

export {
  InfoConf,
  MotifConf,
  DescriptionConf
}
