import React from 'react';
import Header from '../components/Header';

const PageLayout = Page => props => (
  <section className="hero is-fullheight is-danger">
    <Header />
    <Page {...props} />
  </section>
);

export default PageLayout;
