import React from 'react';

const FormLayout = Form => props => {
  return (
    <div className="hero-body has-background-white has-text-black">
      <div className="container">
        <Form {...props} />
      </div>
    </div>
  );
};

export default FormLayout;
