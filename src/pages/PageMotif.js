import React, { Fragment } from 'react';
import { reduxForm } from 'redux-form';
import ButtonsGroup from '../components/ButtonsGroup';
import { MotifConf } from '../customersConf/Nissan';
import { translate } from 'react-i18next';

let PageMotif = ({ previousPage, submitting, handleSubmit, t }) => {
  const buttonOne = {
    type: 'button',
    className: 'button',
    onClick: previousPage,
    text: t('prev')
  };
  const buttonTwo = {
    type: 'submit',
    className: 'button is-primary',
    disabled: submitting, // pristine ?
    text: t('next')
  };
  return (
    <Fragment>
      <h1 className="title has-text-black">{t('header-intervention')}</h1>
      <form onSubmit={handleSubmit}>
        <MotifConf />

        <ButtonsGroup
          twoButton={true}
          buttonOne={buttonOne}
          buttonTwo={buttonTwo}
        />
      </form>
    </Fragment>
  );
};

PageMotif = reduxForm({
  form: 'formulaire',
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(PageMotif);

PageMotif = translate()(PageMotif)

export default PageMotif;
