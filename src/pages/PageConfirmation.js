import React, { Fragment } from 'react';
import { Field, reduxForm } from 'redux-form';
import ButtonsGroup from '../components/ButtonsGroup';
import { translate } from 'react-i18next';

let PageConfirmation = ({t}) => (
  <Fragment>
    <h1 className="title has-text-danger has-text-centered">
      {t('done.saved-request')}
    </h1>
    <br />
    <h2 className="subtitle is-5 has-text-black has-text-centered">
      {t('done.ending-message')}
    </h2>
  </Fragment>
);

PageConfirmation = reduxForm({
  form: 'formulaire',
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(PageConfirmation);

PageConfirmation = translate()(PageConfirmation)

export default PageConfirmation;
