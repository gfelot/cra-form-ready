import React, { Component, Fragment } from 'react';
import { reduxForm } from 'redux-form';
import ButtonsGroup from '../components/ButtonsGroup';
import Modal from '../components/Modal';
import { translate } from 'react-i18next';

class PageCGU extends Component {

  state = {
    showModal: false
  }

  render() {
    const { previousPage, submitting, handleSubmit, t } = this.props;

    const toogleShowModal = () => {
      this.setState((prev, props) => {
        const newState = !prev.showModal;

        return { showModal: newState };
      })
    }

    const buttonOne = {
      type: 'button',
      className: 'button is-danger',
      onClick: previousPage,
      text: t('cgu.refuse')
    };
    const buttonTwo = {
      type: 'submit',
      className: 'button is-primary',
      disabled: submitting, // pristine ?
      text: t('cgu.accept')
    };
    return (
      <Fragment>
        {
          this.state.showModal && <Modal onClick={toogleShowModal} />
        }
        <form onSubmit={handleSubmit}>
          <p className="title is-3 has-text-centered has-text-black">
            {t('cgu.must-accept')}
          </p>
          <br />
          <p className="subtitle has-text-info has-text-centered has-text-weight-bold">
            {t('cgu.continue')}
        </p>
          <ButtonsGroup
            twoButton={true}
            buttonOne={buttonOne}
            buttonTwo={buttonTwo}
          />
          <p className="title is-4 has-text-centered has-text-black">
            <u onClick={toogleShowModal} style={{ cursor: 'pointer' }}>{t('cgu.read')}</u>
          </p>
        </form>
      </Fragment>
    );
  }
}

PageCGU = reduxForm({
  form: 'formulaire',
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(PageCGU);

PageCGU = translate()(PageCGU);

export default PageCGU;
