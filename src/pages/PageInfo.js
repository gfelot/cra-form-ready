import React, { Fragment } from 'react';
import { reduxForm } from 'redux-form';
import ButtonsGroup from '../components/ButtonsGroup';
import { InfoConf } from '../customersConf/Nissan';
import { translate } from 'react-i18next';

let PageInfo = ({ handleSubmit, submitting, t }) => {

  const buttonTwo = {
    type: 'submit',
    className: 'button is-primary',
    disabled: submitting, // pristine ?
    text: t('next')
  };

  return (
    <Fragment>
      <h1 className="title has-text-black">{t('header-contrat')}</h1>
      <form onSubmit={handleSubmit}>
        <InfoConf />

        <ButtonsGroup twoButton={false} buttonTwo={buttonTwo} />
      </form>
    </Fragment>
  );
};

PageInfo = reduxForm({
  form: 'formulaire',
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(PageInfo);

PageInfo = translate()(PageInfo)

export default PageInfo;
