import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import ButtonsGroup from '../components/ButtonsGroup';
import { translate } from 'react-i18next';

let PageRecap = ({
  previousPage,
  submitting,
  handleSubmit,
  prenom,
  nom,
  tel,
  motif,
  immatriculation,
  localisation,
  vehiculeSousSol,
  kilometrage,
  t
}) => {

  const buttonOne = {
    type: 'button',
    className: 'button',
    onClick: previousPage,
    text: t('prev')
  };
  const buttonTwo = {
    type: 'submit',
    className: 'button is-primary',
    disabled: submitting, // pristine ?
    text: t('validate')
  };
  return (
    <Fragment>
      <form onSubmit={handleSubmit}>
        <h1 className="title has-text-black">{t('header-intervention')}</h1>
        <div className="card">
          <div className="card-content">
            <p>{t('name')} : {nom}</p>
            <p>{t('firstname')} : {prenom}</p>
            <p>{t('phone')} : {tel}</p>
            <p>{t('context')} : {motif}</p>
            <p>{t('immat')} : {immatriculation}</p>
            <p>{t('loc')} : {localisation}</p>
            <p>{t('undergroundCarPark')} : {vehiculeSousSol}</p>
          </div>
        </div>
        <br />
        <ButtonsGroup
          twoButton={true}
          buttonOne={buttonOne}
          buttonTwo={buttonTwo}
        />
      </form>
    </Fragment>
  );
};

const selector = formValueSelector('formulaire');
PageRecap = connect(state => {
  const {
    prenom,
    nom,
    tel,
    motif,
    immatriculation,
    localisation,
    vehiculeSousSol,
    kilometrage
  } = selector(
    state,
    'prenom',
    'nom',
    'tel',
    'motif',
    'immatriculation',
    'localisation',
    'vehiculeSousSol',
    'kilometrage'
  );
  return {
    prenom,
    nom,
    tel,
    motif,
    immatriculation,
    localisation,
    vehiculeSousSol,
    kilometrage
  };
})(PageRecap);

PageRecap = reduxForm({
  form: 'formulaire',
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(PageRecap);

PageRecap = translate()(PageRecap)

export default PageRecap;
