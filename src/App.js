import React from 'react';
import PageLayout from "./layouts/PageLayout";
import FormContainer from "./container/FormContainer";
import './App.css';

const App = () => {
    const voirResultat = (values) => {
        console.log(values);
        
    }
    return (
        <FormContainer onSubmit={voirResultat}/>
    )
}

export default PageLayout(App);
